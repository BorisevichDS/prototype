﻿using System;

namespace Prototype
{
    /// <summary>
    /// Интерфейс клонирования объектов.
    /// </summary>
    internal interface IMyCloneable<T>
    {
        T SoftClone();
    }
}
