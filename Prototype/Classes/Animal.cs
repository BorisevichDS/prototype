﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    internal class Animal : Creature, IMyCloneable<Animal>
    {
        public enum Color
        {
            White,
            Black,
            Green,
            Red,
            Pink
        }

        /// <summary>
        /// Цвет шерсти.
        /// </summary>
        public Color HairColor { get; set; }

        public new Animal SoftClone()
        {
            var creature = base.SoftClone();
            Animal animal = new Animal() { Name = creature.Name, HairColor = this.HairColor };
            return animal;
        }

        public override object Clone()
        {
            var creature = base.Clone() as Creature;
            Animal animal = new Animal() { Name = creature.Name, HairColor = this.HairColor };
            return animal;
        }

        public override string ToString()
        {
            return $"Name: {Name}, haircolor: {HairColor}";
        }

    }

}
