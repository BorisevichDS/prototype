﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    /// <summary>
    /// Птица.
    /// </summary>
    internal class Bird : Animal, IMyCloneable<Bird>
    {
        /// <summary>
        /// Признак того умеет ли птица летать или нет.
        /// </summary>
        public bool CanFly { get; set; }

        public new Bird SoftClone()
        {
            Animal animal = base.SoftClone();
            Bird bird = new Bird()
            {
                CanFly = this.CanFly,
                Name = animal.Name,
                HairColor = animal.HairColor
            };
            return bird;
        }

        public override object Clone()
        {
            Animal animal = base.Clone() as Animal;
            Bird bird = new Bird()
            {
                CanFly = this.CanFly,
                Name = animal.Name,
                HairColor = animal.HairColor
            };
            return bird;
        }

        public override string ToString()
        {
            return $"Name: {Name}, haircolor: {HairColor}, canfly: {CanFly}";
        }

    }
}
