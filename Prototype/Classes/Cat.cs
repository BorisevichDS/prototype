﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    /// <summary>
    /// Кошка.
    /// </summary>
    class Cat : Animal, IMyCloneable<Cat>
    {
        /// <summary>
        /// Признак наличия хвоста.
        /// </summary>
        public bool HasTail { get; set; }

        public new Cat SoftClone()
        {
            var animal = base.SoftClone();
            Cat cat = new Cat()
            {
                Name = animal.Name,
                HairColor = animal.HairColor,
                HasTail = this.HasTail
            };
            return cat;
        }

        public override object Clone()
        {
            var animal = base.Clone() as Animal;
            Cat cat = new Cat()
            {
                Name = animal.Name,
                HairColor = animal.HairColor,
                HasTail = this.HasTail
            };
            return cat;
        }

        public override string ToString()
        {
            return $"Name: {Name}, haircolor: {HairColor}, hastail: {HasTail}";
        }
    }
}
