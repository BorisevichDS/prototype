﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    /// <summary>
    /// Существо.
    /// </summary>
    internal class Creature : IMyCloneable<Creature>, ICloneable
    {
        /// <summary>
        /// Имя существа.
        /// </summary>
        public string Name { get; set; }

        public Creature SoftClone()
        {
            return new Creature() { Name = this.Name };
        }

        public virtual object Clone()
        {
            return new Creature() { Name = this.Name };
        }

        public override string ToString()
        {
            return $"Name: {Name}";
        }
    }
}
