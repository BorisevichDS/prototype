﻿using System;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Animal a1 = new Animal() { Name = "Kitty", HairColor = Animal.Color.Green };
                Animal a2 = a1.SoftClone() as Animal;

                a2.HairColor = Animal.Color.Red;
                Console.WriteLine($"Оригинал -> {a1}");
                Console.WriteLine($"Копия    -> {a2}");

                Cat cat1 = new Cat() { Name = "Fluffy", HairColor = Animal.Color.White, HasTail = true };
                var cat2 = cat1.SoftClone() as Cat;

                cat2.HasTail = false;

                Console.WriteLine($"Оригинал -> {cat1}");
                Console.WriteLine($"Копия    -> {cat2}");

                Bird bird1 = new Bird() { Name = "Cherry", HairColor = Animal.Color.Pink, CanFly = true };
                var bird2 = bird1.SoftClone() as Bird;

                bird2.HairColor = Animal.Color.Green;

                Console.WriteLine($"Оригинал -> {bird1}");
                Console.WriteLine($"Копия    -> {bird2}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
